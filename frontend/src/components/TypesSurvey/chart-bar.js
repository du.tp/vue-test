import { Bar } from "vue-chartjs";
export const OPTIONS_CHART = {
  responsive: true,
  maintainAspectRatio: false,
  scales: {
    yAxes: [
      {
        ticks: {
          min: 0,
          max: 10,
          stepSize: 1,
          reverse: false,
          beginAtZero: true,
        },
      },
    ],
  },
};
export default {
  extends: Bar,
  props: {
    dataChart: {
      type: Object,
      default: () => {},
    },
  },
  data: () => ({
    chartDatas: {
      labels: [],
      datasets: [
        {
          label: "Best sellers",
          data: [],
          backgroundColor: "#f87979",
        },
      ],
    },
  }),
  watch: {
    dataChart: {
      deep: true,
      handler() {
        this.getDataChart();
        this.$data._chart.update();
      },
    },
  },
  mounted() {
    for (const item in this.dataChart) {
      this.chartDatas.labels.push(item);
    }
    this.getDataChart();
    this.renderChart(this.chartDatas, OPTIONS_CHART);
  },
  methods: {
    getDataChart() {
      let datas = [];
      for (const item in this.dataChart) {
        datas.push(this.dataChart[item]);
      }
      this.chartDatas.datasets[0].data = datas;
    },
  },
};
