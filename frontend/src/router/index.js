import Vue from "vue";
import VueRouter from "vue-router";

import Surveys from "@/views/surveys";
import SurveyDetail from "@/views/survey-detail";
import NotFound from "@/views/404";

Vue.use(VueRouter);

export const routes = [
  {
    path: "/",
    name: "Surveys",
    component: Surveys,
  },
  {
    path: "/detail/:id",
    name: "SurveyDetail",
    component: SurveyDetail,
  },
  {
    path: "*",
    name: "NotFound",
    component: NotFound,
  },
];

const router = new VueRouter({
  mode: "history",
  routes,
});

export default router;
