const path = require("path");

function resolve(dir) {
  return path.join(__dirname, dir);
}
// Page title
const name = "Bocasay survey";

module.exports = {
  publicPath: "/",
  outputDir: "dist",
  configureWebpack: {
    name,
    devtool: "source-map",
    resolve: {
      alias: {
        "@": resolve("src"),
      },
    },
  },
  css: {
    loaderOptions: {
      sass: {
        additionalData: `@import "@/assets/styles/variables.scss";`,
      },
    },
  },
};
